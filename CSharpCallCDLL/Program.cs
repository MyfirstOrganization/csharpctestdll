﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
namespace CSharpCallDLL
{
    class Program
    {
        //[DllImport(@"E:/PLC_example/CsharpTC/x64/Release/CreateDLL.dll", EntryPoint = "test01")]
        //static extern int test01(int a, int b, int c);
        //[DllImport(@"E:/PLC_example/CsharpTC/x64/Release/CreateDLL.dll", EntryPoint = "test02")]
        //static extern int test02(int a, int b);
        //[DllImport(@"E:/PLC_example/CsharpTC/x64/Release/CreateDLL.dll", EntryPoint = "test03")]
        //static extern int test03();
        //[DllImport(@"E:/PLC_example/CsharpTC/x64/Release/CreateDLL.dll", EntryPoint = "Create")]
        //static extern IntPtr Create(string name,int age);

        //[StructLayout(LayoutKind.Sequential)]
        //public struct User
        //{
        //    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        //    public string Name;
        //    public int Age;

        //}

        ////其中这里的结构User就和C++中的User对应。
        //[DllImport("E:/PLC_example/CsharpTC/x64/Release/CreateDLL.dll", EntryPoint = "GetArray")]
        //static extern bool GetArray(int ElementNumber, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)]double[] BaseAddress);

        //[DllImport("E:/PLC_example/CsharpTC/x64/Release/CreateDLL.dll", EntryPoint = "GetArrayElementNumber")]
        //static extern int GetArrayElementNumber();

        //[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        //public struct PT
        //{
        //    public float x;
        //    public float y;
        //    public float z;
        //    public PT(float xx,float yy,float zz)
        //    {
        //        x = xx;
        //        y = yy;
        //        z = zz;
        //    }
        //}
        //[DllImport("E:/PLC_example/CsharpTC/x64/Release/CreateDLL.dll", EntryPoint = "GetPointClouds")]
        //static extern bool GetPointClouds(IntPtr pts);
        //[DllImport("E:/PLC_example/CsharpTC/x64/Release/CreateDLL.dll", EntryPoint = "SetPointClouds")]
        //static extern bool SetPointClouds(IntPtr InPoints, int number,IntPtr OutPoints);

        [DllImport(@"CTestDLL.dll", EntryPoint = "test01")]
        static extern int test01(int a, int b, int c);
        [DllImport(@"CTestDLL.dll", EntryPoint = "test02")]
        static extern int test02(int a, int b);
        [DllImport(@"CTestDLL.dll", EntryPoint = "test03")]
        static extern int test03();
        [DllImport(@"CTestDLL.dll", EntryPoint = "Create")]
        static extern IntPtr Create(string name, int age);

        [StructLayout(LayoutKind.Sequential)]
        public struct User
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string Name;
            public int Age;

        }

        //其中这里的结构User就和C++中的User对应。
        [DllImport("CTestDLL.dll", EntryPoint = "GetArray")]
        static extern bool GetArray(int ElementNumber, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)]double[] BaseAddress);

        [DllImport("CTestDLL.dll", EntryPoint = "GetArrayElementNumber")]
        static extern int GetArrayElementNumber();

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct Point3DF32
        {
            public float x;
            public float y;
            public float z;
            public Point3DF32(float xx, float yy, float zz)
            {
                x = xx;
                y = yy;
                z = zz;
            }
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct STLMesh
        {
            Point3DF32 normal;
            [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.Struct)]
            Point3DF32[] vertexs;
        }
        //[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        //public struct STLMesh
        //{
        //    long size;
        //    [MarshalAs(UnmanagedType.SysUInt)]
        //    IntPtr ptr_triangles;

        //}
        [DllImport("CTestDLL.dll", EntryPoint = "GetPointClouds")]
        static extern bool GetPointClouds(IntPtr pts);
        [DllImport("CTestDLL.dll", EntryPoint = "SetPointClouds")]
        static extern bool SetPointClouds(IntPtr InPoints, int number, IntPtr OutPoints);
        [DllImport("CTestDLL.dll", EntryPoint = "StatisticalOutlierRemovalFilter")]
        static extern int StatisticalOutlierRemovalFilter(IntPtr InPoints, int number, IntPtr OutPoints);

        [DllImport("CTestDLL.dll", EntryPoint = "PassThroughFilter")]
        static extern int PassThroughFilter(IntPtr InPoints, int number, IntPtr OutPoints);
        [DllImport("CTestDLL.dll", EntryPoint = "GetPolygonMesh")]
        static extern IntPtr GetPolygonMesh(IntPtr InPoints,int number,ref int count);
        [DllImport("CTestDLL.dll", EntryPoint = "MovingLeastSquaresSmooth")]
        static extern int MovingLeastSquaresSmooth(IntPtr InPoints, int number, IntPtr OutPoints);
        [DllImport("CTestDLL.dll", EntryPoint = "Extract_Plain")]
        static extern int Extract_Plain(IntPtr InPoints, int number, IntPtr OutPoints);
        [DllImport("CTestDLL.dll", EntryPoint = "UpsamplingToMesh")]
        static extern int UpsamplingToMesh(IntPtr InPoints, int number);
        [DllImport("CTestDLL.dll", EntryPoint = "GetMarchingCubesPolygonMesh")]
        static extern int GetMarchingCubesPolygonMesh(IntPtr InPoints, int number);
        [DllImport("CTestDLL.dll", EntryPoint = "GetPoissonPolygonMesh")]
        static extern int GetPoissonPolygonMesh(IntPtr InPoints, int number);
        [DllImport("CTestDLL.dll", EntryPoint = "GetGridProjectionPolygonMesh")]
        static extern int GetGridProjectionPolygonMesh(IntPtr InPoints, int number);

        

        static void Main(string[] args)
        {
            int r1 = test01(1, 2, 3);
            int r2 = test02(5, 2);
            int r3 = test03();
            List<Point3DF32> points = new List<Point3DF32>();

           
            IntPtr ptr1 = Create("李平",27);
            User user = (User)Marshal.PtrToStructure(ptr1, typeof(User));
            //这里结构指针首先转换成IntPtr句柄，然后通过Marshal.PtrToStructrue转换成你所需要的结构
            int ElementNumber = GetArrayElementNumber();
            double[] doubleArray = new double[ElementNumber];
            GetArray(ElementNumber, doubleArray);
            //读取点云数据
            string txtFilename = "E:/PLC_example/CTestDLL/box.txt"; //txt文件放置的位置
            points = getArrayFromTxt(txtFilename);
            
            int number = points.Count;//这个有读取的txt文档中返回的行数
            int size1 = Marshal.SizeOf(typeof(Point3DF32)) * number;
            IntPtr pt = Marshal.AllocHGlobal(size1);
            //实现将存储点云的结构体数组输入到C++中去处理
            Point3DF32[] pts = new Point3DF32[number];
            for (int j=0;j< number; j++)
            {
                
                pts[j].x = points[j].x;
                pts[j].y = points[j].y;
                pts[j].z = points[j].z;

            }
            DateTime dt1 = System.DateTime.Now;
            // SetPointClouds(Marshal.UnsafeAddrOfPinnedArrayElement(pts, 0), number, pt);
            //int num= StatisticalOutlierRemovalFilter(Marshal.UnsafeAddrOfPinnedArrayElement(pts, 0), number, pt);
            //int num= PassThroughFilter(Marshal.UnsafeAddrOfPinnedArrayElement(pts, 0), number, pt);
            int size2 = Marshal.SizeOf(typeof(STLMesh)) * 100000;
            IntPtr mesh = Marshal.AllocHGlobal(size2);
            int count=0;

            IntPtr meshpt = GetPolygonMesh(Marshal.UnsafeAddrOfPinnedArrayElement(pts, 0),number,ref count);

            List<STLMesh> stlmesh = new List<STLMesh>();
            for (int i = 0; i < count; i++)
            {
                IntPtr ptr = new IntPtr(meshpt.ToInt64() + Marshal.SizeOf(typeof(STLMesh)) * i);
                stlmesh.Add((STLMesh)Marshal.PtrToStructure(ptr, typeof(STLMesh)));
               
            }

            // int num1 = UpsamplingToMesh(Marshal.UnsafeAddrOfPinnedArrayElement(pts, 0), number);
            // int num1 = GetMarchingCubesPolygonMesh(Marshal.UnsafeAddrOfPinnedArrayElement(pts, 0), number);
            // int num1 = GetPoissonPolygonMesh(Marshal.UnsafeAddrOfPinnedArrayElement(pts, 0), number);

            //int num1 = GetGridProjectionPolygonMesh(Marshal.UnsafeAddrOfPinnedArrayElement(pts, 0), number);

            // int num2 = MovingLeastSquaresSmooth(Marshal.UnsafeAddrOfPinnedArrayElement(pts, 0), number, pt);
            //int num =Extract_Plain(Marshal.UnsafeAddrOfPinnedArrayElement(pts, 0), number, pt);
            //实现接受c++处理过后的点云（结构体数组）
            //源目标参数
            //int size1 = Marshal.SizeOf(typeof(Point3DF32)) * 1000;
            //IntPtr pt = Marshal.AllocHGlobal(size1);
            // GetPointClouds(pt);
            //还原成结构体数组  
            Console.WriteLine("Numbers of source points are {0}",pts.Length);
            //Point3DF32[] FilteredPoints = new Point3DF32[num];//存放滤波后的点云
           // List<Point3DF32> FilteredPoints = new List<Point3DF32>();
           // for (int i = 0; i < num; i++)
           // {
           //     IntPtr ptr = new IntPtr(pt.ToInt64() + Marshal.SizeOf(typeof(Point3DF32)) * i);
           //     FilteredPoints.Add((Point3DF32)Marshal.PtrToStructure(ptr, typeof(Point3DF32)));
           //   //  Console.WriteLine("x:{0}, y:{1}, z:{2}", pts[i].x, pts[i].y, pts[i].z);
           //}
           // Console.WriteLine("Numbers of filtered points are {0} ", FilteredPoints.Count);
            Console.WriteLine("Numbers of filtered points are {0} ", count);
            string fileName = string.Format("PCD-{0}.txt", DateTime.Now.ToString("YYYYMMddHHmmss"));//保存txt文件名
            string dirPath = "E:/PLC_example/CsharpTC/";
            string filePath = Path.Combine(dirPath, fileName);
            //SavePCD(FilteredPoints,filePath,true);

            Marshal.FreeHGlobal(pt);//释放内存
            //Console.WriteLine("test01的结果：" + r1.ToString());
            //Console.WriteLine("test02的结果：" + r2.ToString());
            //Console.WriteLine("test03的结果：" + r3.ToString());
            //Console.WriteLine("Name:{0},Age:{1}",user.Name,user.Age);
            //Console.WriteLine("数组的元素个数"+ElementNumber);
            DateTime dt2 = System.DateTime.Now;
            TimeSpan ts = dt2.Subtract(dt1);
            Console.WriteLine("elasp time {0}", ts.TotalMilliseconds / 1000);
            Console.ReadKey();
        }

        //定义方法的地方
        //从txt文档中读入数据
        public static List<Point3DF32> getArrayFromTxt(string filePath)
        {

            List<Point3DF32> point3dList = new List<Point3DF32>();
            using (StreamReader sr = new StreamReader(filePath as string, Encoding.Default))
            {
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    try
                    {
                        line = line.Trim();
                        line = new System.Text.RegularExpressions.Regex("[\\s]+").Replace(line, " ");
                        string[] xyzrgb = line.Trim().Split(' ');
                        float x = float.Parse(xyzrgb[0]) / 1000.0f;
                        float y = float.Parse(xyzrgb[1]) / 1000.0f;
                        float z = float.Parse(xyzrgb[2]) / 1000.0f;
                        point3dList.Add(new Point3DF32(x, y, z));
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            return point3dList;
        }
        //保存滤波后的点云到txt文件中
        //filePath=path+filename;
        public static void SavePCD(List<Point3DF32> pcdList, string filePath, bool isSaveRGB)
        {
            try
            {
                FileInfo finfo = new FileInfo(filePath);
                string dirPath = finfo.DirectoryName;
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
                StreamWriter sw = new StreamWriter(filePath, true);
                if (isSaveRGB)
                {

                    foreach (Point3DF32 p in pcdList)
                    {
                        string str = string.Format("{0} {1} {2} {3} {4} {5}", p.x, p.y, p.z, 255, 0, 0);
                        sw.WriteLine(str);
                    }
                }
                else
                {

                    foreach (Point3DF32 p in pcdList)
                    {
                        string str = string.Format("{0} {1} {2}", p.x, p.y, p.z);
                        sw.WriteLine(str);
                    }
                }


                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                
            }
        }


    }   
}


