#include "stdafx.h"
#include "CtestDLL.h"

using namespace pcl;
using namespace std;

UserInfo* userInfo;


extern "C" __declspec(dllexport) User* Create(char* name, int age)
{
	User* user = (User*)malloc(sizeof(User));

	userInfo = new UserInfo(name,age);
	strcpy(user->name,userInfo->GetName());
	user->age = userInfo->GetAge();
	//strcpy(user->name, name);
	//user->age = age;

	return user;
}
int __stdcall  test01(int a, int b, int c)
{
	return a + b + c;
}
int __stdcall  test02(int a, int b)
{
	return a - b;
}

int __stdcall  test03()
{
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

	// Generate pointcloud data
	cloud->width = 1000;
	cloud->height = 1;
	cloud->points.resize(cloud->width * cloud->height);

	for (size_t i = 0; i < cloud->points.size(); ++i)
	{
		cloud->points[i].x = 1024.0f * rand() / (RAND_MAX + 1.0f);
		cloud->points[i].y = 1024.0f * rand() / (RAND_MAX + 1.0f);
		cloud->points[i].z = 1024.0f * rand() / (RAND_MAX + 1.0f);
	}
	return cloud->points.size();

}


int StaticElementNumber = 10;
extern "C" _declspec(dllexport) bool GetArray(int ElementNumber, double *BaseAddress)
 {
	     if (ElementNumber < StaticElementNumber)
		     {
		         return false;
		     }
	
		     for (int i = 0; i < StaticElementNumber; ++i)
		     {
		         BaseAddress[i] = 1 / ((double)i + 1);
		     }
	
		     return true;
	 }

 extern "C" _declspec(dllexport) int GetArrayElementNumber()
 {
	     return StaticElementNumber;
}

 extern "C" _declspec(dllexport) bool GetPointClouds(Point3DF32* pts)
 {
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

	 // Generate pointcloud data
	 cloud->width = 1000;
	 cloud->height = 1;
	 cloud->points.resize(cloud->width * cloud->height);

	 for (size_t i = 0; i < cloud->points.size(); ++i)
	 {
		 cloud->points[i].x = 1024.0f * rand() / (RAND_MAX + 1.0f);
		 cloud->points[i].y = 1024.0f * rand() / (RAND_MAX + 1.0f);
		 cloud->points[i].z = 1024.0f * rand() / (RAND_MAX + 1.0f);
	 }

	 for (size_t j = 0; j < cloud->points.size(); j++)
	 {
		 
		 pts[j].x = cloud->points[j].x;
		 pts[j].y = cloud->points[j].y;
		 pts[j].z = cloud->points[j].z;
	 }
	 return true;

 }

 extern "C" _declspec(dllexport) bool SetPointClouds(Point3DF32* InPoints, int number, Point3DF32* OutPoints)
 {
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	 cloud->points.resize(number);

	 for (size_t i = 0; i < number; ++i)
	 {
		 cloud->points[i].x = InPoints[i].x;
		 cloud->points[i].y = InPoints[i].y;
		 cloud->points[i].z = InPoints[i].z;
	 }

	 for (size_t j = 0; j < cloud->points.size(); ++j)
	 {
		 OutPoints[j].x= cloud->points[j].x ;
		 OutPoints[j].y= cloud->points[j].y ;
		 OutPoints[j].z= cloud->points[j].z ;
	 }

	 return true;
 }

 //点云滤波函数，返回PT类型的指针
 //统计学滤波StatisticalOutlierRemoval
 extern "C" _declspec(dllexport) int StatisticalOutlierRemovalFilter(Point3DF32* InPoints,int number, Point3DF32* OutPoints)
 {
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);//滤波后的点云
	 //将PT类型的指针转换成PointXYZ类型
	 cloud->points.resize(number);
	 for (size_t i=0;i<number;i++)
	 {
		 cloud->points[i].x = InPoints[i].x;
		 cloud->points[i].y = InPoints[i].y;
		 cloud->points[i].z = InPoints[i].z;
	 }

	 pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
	 sor.setInputCloud(cloud);
	 sor.setMeanK(30);
	 sor.setStddevMulThresh(0.8);
	 sor.filter(*cloud_filtered);
	 //将PointXYZ类型转换成PT类型
	 for (size_t i=0;i<cloud_filtered->points.size();i++)
	 {
		 OutPoints[i].x = cloud_filtered->points[i].x;
		 OutPoints[i].y = cloud_filtered->points[i].y;
		 OutPoints[i].z = cloud_filtered->points[i].z;

	 }
	 return cloud_filtered->points.size();
 
 }
 //passthrough直通滤波器
 extern "C" _declspec(dllexport) int PassThroughFilter(Point3DF32* InPoints, int number, Point3DF32* OutPoints)
 {
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);//滤波后的点云
																							//将PT类型的指针转换成PointXYZ类型
	 cloud->points.resize(number);
	 for (size_t i = 0; i<number; i++)
	 {
		 cloud->points[i].x = InPoints[i].x;
		 cloud->points[i].y = InPoints[i].y;
		 cloud->points[i].z = InPoints[i].z;
	 }
	  
	 pcl::PassThrough<pcl::PointXYZ> pass;
	 pass.setInputCloud(cloud);
	 pass.setFilterFieldName("z");//z轴方向上滤波
	 pass.setFilterLimits(200,700);//范围在0--350m
	 pass.setFilterFieldName("x");
	 pass.setFilterLimits(-200.0, 200.0);
	 pass.setFilterLimitsNegative(false);
	 pass.filter(*cloud_filtered);
	 	 
	 //将PointXYZ类型转换成PT类型
	 for (size_t i = 0; i<cloud_filtered->points.size(); i++)
	 {
		 OutPoints[i].x = cloud_filtered->points[i].x;
		 OutPoints[i].y = cloud_filtered->points[i].y;
		 OutPoints[i].z = cloud_filtered->points[i].z;

	 }
	 return cloud_filtered->points.size();

 }
 //生成mesh三角面片，并保存成stl文件
 extern "C" _declspec(dllexport) STLMesh* GetPolygonMesh(Point3DF32* InPoints,int number, int* count)
 {
	 //load input Points
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_smoothed(new pcl::PointCloud<pcl::PointXYZ>);//原点云

																				   //将PT类型的指针转换成PointXYZ类型
	 cloud->points.resize(number);
	 for (size_t i = 0; i<number; i++)
	 {
		 cloud->points[i].x = InPoints[i].x;
		 cloud->points[i].y = InPoints[i].y;
		 cloud->points[i].z = InPoints[i].z;
	 }
	 //Resample:VoxelGrid // Create the filtering object
	 pcl::VoxelGrid<pcl::PointXYZ> vol;
	 vol.setInputCloud(cloud);
	 vol.setLeafSize(0.002f,0.002f,0.002f);
	 vol.filter(*cloud_filtered);
	 MovingLeastSquares<PointXYZ, PointXYZ> mls1;
	 mls1.setInputCloud(cloud_filtered);
	 mls1.setSearchRadius(0.03);
	 mls1.setPolynomialFit(true);
	 mls1.setPolynomialOrder(4);
	 mls1.process(*cloud_smoothed);
	 //采用MovingLeastSquares方法进行平滑,并进行法向量估计
	 pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals(new pcl::PointCloud<pcl::PointNormal>);
	 pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> mls;
	 mls.setComputeNormals(true);
	 mls.setInputCloud(cloud_smoothed);
	 mls.setPolynomialFit(true);
	 mls.setPolynomialOrder(4);
	 mls.setSearchRadius(0.01); 
	
	 mls.process(*cloud_with_normals);

	 // Create search tree*
	 pcl::search::KdTree<pcl::PointNormal>::Ptr tree2(new pcl::search::KdTree<pcl::PointNormal>);
	 tree2->setInputCloud(cloud_with_normals);
	 // Initialize objects
	 pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
	 pcl::PolygonMesh triangles;
	 // Set the maximum distance between connected points (maximum edge length)
	 gp3.setSearchRadius(10);
	 
	 // Set typical values for the parameters
	 gp3.setMu(3);
	 gp3.setMaximumNearestNeighbors(100);
	 gp3.setMaximumSurfaceAngle(M_PI / 2); // 90 degrees
	 gp3.setMinimumAngle(M_PI /180); // 1 degrees
	 gp3.setMaximumAngle(2 * M_PI / 2); // 180 degrees
	 gp3.setNormalConsistency(true);
	 gp3.setConsistentVertexOrdering(true);
	 // Get result
	 gp3.setInputCloud(cloud_with_normals);
	 gp3.setSearchMethod(tree2);
	 gp3.reconstruct(triangles);
	 //将mesh三角面片保存为stl文件
	 // pcl::io::savePolygonFileSTL("E:/PLC_example/CTestDLL/test.stl",triangles,false);
	 // Additional vertex information
	 // std::vector<int> parts = gp3.getPartIDs();
	 // std::vector<int> states = gp3.getPointStates();
	 //提取三角面片的三个顶点和方向量

	 unsigned int nr_points = triangles.cloud.width * triangles.cloud.height;//点数
	 unsigned int nr_polygons = static_cast<unsigned int> (triangles.polygons.size());//三角面片数
	 //vector<STLMesh> stlmesh=new vector<STLMesh>;

	 //STLMesh *stlmesh = new STLMesh[1];
	 //stlmesh[0].triangles= new STLtriangles[nr_polygons];
	 STLMesh *stlmesh= new STLMesh[nr_polygons];
	 Point3DF32 *p3d = new Point3DF32[nr_points];
	 Point3DF32 *p3d_normals = new Point3DF32[nr_points];
	 // get field indices for x, y, z (as well as rgb and/or rgba)
	 int idx_x = -1, idx_y = -1, idx_z = -1, idx_rgb = -1, idx_rgba = -1, idx_normal_x = -1, idx_normal_y = -1, idx_normal_z = -1;
	 for (int d = 0; d < static_cast<int> (triangles.cloud.fields.size()); ++d)
	 {
		 if (triangles.cloud.fields[d].name == "x") idx_x = d;
		 else if (triangles.cloud.fields[d].name == "y") idx_y = d;
		 else if (triangles.cloud.fields[d].name == "z") idx_z = d;
		 else if (triangles.cloud.fields[d].name == "rgb") idx_rgb = d;
		 else if (triangles.cloud.fields[d].name == "rgba") idx_rgba = d;
		 else if (triangles.cloud.fields[d].name == "normal_x") idx_normal_x = d;
		 else if (triangles.cloud.fields[d].name == "normal_y") idx_normal_y = d;
		 else if (triangles.cloud.fields[d].name == "normal_z") idx_normal_z = d;
	 }
	 if ((idx_x == -1) || (idx_y == -1) || (idx_z == -1))
		 nr_points = 0;
	 // copy point data
	if (nr_points > 0)
	 {
		 Eigen::Vector4f pt = Eigen::Vector4f::Zero();
		 Eigen::Array4i xyz_offset(triangles.cloud.fields[idx_x].offset, triangles.cloud.fields[idx_y].offset, triangles.cloud.fields[idx_z].offset, 0);
		 float nx = 0.0f, ny = 0.0f, nz = 0.0f;
		 for (long cp = 0; cp < nr_points; ++cp, xyz_offset += triangles.cloud.point_step)
		 {
			 memcpy(&pt[0], &triangles.cloud.data[xyz_offset[0]], sizeof(float));
			 memcpy(&pt[1], &triangles.cloud.data[xyz_offset[1]], sizeof(float));
			 memcpy(&pt[2], &triangles.cloud.data[xyz_offset[2]], sizeof(float));
			 memcpy(&nx, &triangles.cloud.data[cp*triangles.cloud.point_step + triangles.cloud.fields[idx_normal_x].offset], sizeof(float));
			 memcpy(&ny, &triangles.cloud.data[cp*triangles.cloud.point_step + triangles.cloud.fields[idx_normal_y].offset], sizeof(float));
			 memcpy(&nz, &triangles.cloud.data[cp*triangles.cloud.point_step + triangles.cloud.fields[idx_normal_z].offset], sizeof(float));
			 p3d[cp].x = pt[0];
			 p3d[cp].y = pt[1];
			 p3d[cp].z = pt[2];
			 p3d_normals[cp].x = nx;
			 p3d_normals[cp].y = ny;
			 p3d_normals[cp].z = nz;

		 }
	 }
	 // copy polygon data
	 if (nr_polygons > 0)
	 {
		 for (unsigned int i = 0; i < nr_polygons; i++)
		 {
			 for (unsigned int j = 0; j < 3; j++)
			 {
				 stlmesh[i].vertexs[j]=p3d[triangles.polygons[i].vertices[j]];
		 
			 }
			 stlmesh[i].normal = p3d_normals[triangles.polygons[i].vertices[0]];

		 }
	 }
	 *count = nr_polygons;
	return stlmesh;

	// pcl::visualization::PCLVisualizer viewer("viewer");
	// viewer.addPolygonMesh(triangles);
	//// viewer.setRepresentationToWireframeForAllActors();//网格模型以线框图模式显示  
 //    viewer.setRepresentationToSurfaceForAllActors(); //网格模型以面片形式显示
	//// viewer.setRepresentationToPointsForAllActors(); //网格模型以点形式显示  
	// viewer.spin();
	 //return cloud_filtered->points.size();
 }
 //MovingLeastSquares移动最小二乘法点云平滑
 extern "C" _declspec(dllexport) int MovingLeastSquaresSmooth(Point3DF32* InPoints, int number, Point3DF32* OutPoints)
 {
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);//滤波后的点云
																							//将PT类型的指针转换成PointXYZ类型
	 cloud->points.resize(number);
	 for (size_t i = 0; i<number; i++)
	 {
		 cloud->points[i].x = InPoints[i].x;
		 cloud->points[i].y = InPoints[i].y;
		 cloud->points[i].z = InPoints[i].z;
	 }
	 pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
	 pcl::PointCloud<pcl::PointNormal> mls_points;
	 pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
	 // Init object (second point type is for the normals, even if unused)
	 pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> mls;
	 mls.setComputeNormals(true);

	 // Set parameters
	 mls.setInputCloud(cloud);
	 mls.setPolynomialFit(true);
	 mls.setSearchMethod(tree);
	 mls.setSearchRadius(0.03);
	 
	 // Reconstruct
	 mls.process(mls_points);
	// pcl::copyPointCloud(mls_points, *normals);
	 
	 //将PointXYZ类型转换成PT类型
	 for (size_t i = 0; i<cloud_filtered->points.size(); i++)
	 {
		 OutPoints[i].x = mls_points.points[i].x;
		 OutPoints[i].y = mls_points.points[i].y;
		 OutPoints[i].z = mls_points.points[i].z;

	 }
	 return mls_points.points.size();

 }

 //提取点云中的平面
 extern "C" _declspec(dllexport) int Extract_Plain(Point3DF32* InPoints, int number, Point3DF32* OutPoints)
 {
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);//滤波后的点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p(new pcl::PointCloud<pcl::PointXYZ>);
	//将PT类型的指针转换成PointXYZ类型
	 cloud->points.resize(number);
	 for (size_t i = 0; i<number; i++)
	 {
		 cloud->points[i].x = InPoints[i].x;
		 cloud->points[i].y = InPoints[i].y;
		 cloud->points[i].z = InPoints[i].z;
	 }

	 //降采样
	 //pcl::VoxelGrid<pcl::PointXYZ> sor;
	 //sor.setInputCloud(cloud);
	 //sor.setLeafSize(0.001f, 0.001f, 0.001f);//1mm的立方体
	 //sor.filter(*cloud_filtered);

	 pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients());
	 pcl::PointIndices::Ptr inliers(new pcl::PointIndices());
	 pcl::SACSegmentation<pcl::PointXYZ> seg;
	 // Optional
	 seg.setOptimizeCoefficients(true);
	 // Mandatory
	 seg.setModelType(pcl::SACMODEL_PLANE);
	 seg.setMethodType(pcl::SAC_RANSAC);
	 seg.setMaxIterations(1000);
	 seg.setDistanceThreshold(0.004);//到平面的距离在4mm以内都是局内点
	 // Create the filtering object
	 pcl::ExtractIndices<pcl::PointXYZ> extract;
	 seg.setInputCloud(cloud);
	 seg.segment(*inliers, *coefficients);
	 if (inliers->indices.size() == 0)
	 {
		 std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
		 return 0;
	 }
	 // Extract the inliers
	 extract.setInputCloud(cloud);
	 extract.setIndices(inliers);
	 extract.setNegative(true);
	 extract.filter(*cloud_p);
	 std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;
	 //std::stringstream ss;
	 //ss << "boxPlain_plane" << ".pcd";
	// writer.write<pcl::PointXYZ>(ss.str(), *cloud_p, false);
	//将PointXYZ类型转换成PT类型
	 for (size_t i = 0; i<cloud_p->points.size(); i++)
	 {
		 OutPoints[i].x = cloud_p->points[i].x;
		 OutPoints[i].y = cloud_p->points[i].y;
		 OutPoints[i].z = cloud_p->points[i].z;

	 }
	 return cloud_p->points.size();
 
 }
 //对降采样的点云，利用增采样方法进行空洞修补，生成mesh三角面片，并保存成stl文件
 extern "C" _declspec(dllexport) int UpsamplingToMesh(Point3DF32* InPoints, int number)
 {
	 //load input Points
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr VGfiltered(new pcl::PointCloud<pcl::PointXYZ>);//降采样后的点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr UPfiltered(new pcl::PointCloud<pcl::PointXYZ>);//增采样后的点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr Unfiltered(new pcl::PointCloud<pcl::PointXYZ>);//增采样后的点云

																							//将PT类型的指针转换成PointXYZ类型
	 cloud->points.resize(number);
	 for (size_t i = 0; i<number; i++)
	 {
		 cloud->points[i].x = InPoints[i].x;
		 cloud->points[i].y = InPoints[i].y;
		 cloud->points[i].z = InPoints[i].z;
	 }
	 //Resample:VoxelGrid // Create the filtering object
	 pcl::VoxelGrid<pcl::PointXYZ> vol;
	 vol.setInputCloud(cloud);
	 vol.setLeafSize(0.003f, 0.003f, 0.003f);
	 vol.filter(*VGfiltered);
	 //采用增采样填补空洞
	 //滤波对象：
	 pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ> mfilter;
	 mfilter.setInputCloud(VGfiltered);
	 //建立搜索对象
	 pcl::search::KdTree<pcl::PointXYZ>::Ptr kdtree;
	 mfilter.setSearchMethod(kdtree);
	 //设置搜索邻域的半径为3cm
	 mfilter.setSearchRadius(0.03);
	 //upsampling采样的方法有DISTINCT_CLOUD, RANDOM_UNIFORM_DENSITY
	 /*mfilter.setUpsamplingMethod(pcl::MovingLeastSquares<pcl::PointXYZ,pcl::PointXYZ>::SAMPLE_LOCAL_PLANE);
	 mfilter.setUpsamplingRadius(0.03);
	 mfilter.setUpsamplingStepSize(0.02);*/
	 mfilter.setUpsamplingMethod(MovingLeastSquares<PointXYZ, PointXYZ>::VOXEL_GRID_DILATION);
	 mfilter.setDilationVoxelSize(0.005);
	 mfilter.setDilationIterations(100);
	 mfilter.process(*UPfiltered);
	 //Uniform sampling均匀采样
	 /*pcl::UniformSampling<pcl::PointXYZ> unfilter;
	 unfilter.setInputCloud(UPfiltered);
	 unfilter.setRadiusSearch(0.001f);
	 unfilter.filter(*Unfiltered);*/
	 
	 //采用MovingLeastSquares方法进行平滑,并进行法向量估计
	 pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals(new pcl::PointCloud<pcl::PointNormal>);
	 pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> mls;
	 mls.setComputeNormals(true);
	 mls.setInputCloud(Unfiltered);
	 mls.setPolynomialFit(true);
	 mls.setSearchRadius(0.05);	 
	 mls.process(*cloud_with_normals);
	 	 // Create search tree*
	 pcl::search::KdTree<pcl::PointNormal>::Ptr tree2(new pcl::search::KdTree<pcl::PointNormal>);
	 tree2->setInputCloud(cloud_with_normals);
	 // Initialize objects
	 pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
	 pcl::PolygonMesh triangles;
	 // Set the maximum distance between connected points (maximum edge length)
	 gp3.setSearchRadius(5);

	 // Set typical values for the parameters
	 gp3.setMu(3);
	 gp3.setMaximumNearestNeighbors(100);
	 gp3.setMaximumSurfaceAngle(M_PI / 2); // 90 degrees
	 gp3.setMinimumAngle(M_PI / 180); // 1 degrees
	 gp3.setMaximumAngle(2 * M_PI / 2); // 180 degrees
	 gp3.setNormalConsistency(true);
	 gp3.setConsistentVertexOrdering(true);
	 // Get result
	 gp3.setInputCloud(cloud_with_normals);
	 gp3.setSearchMethod(tree2);
	 gp3.reconstruct(triangles);
	 //将mesh三角面片保存为stl文件
	 pcl::io::savePolygonFileSTL("E:/PLC_example/CTestDLL/test.stl", triangles, false);
	 // Additional vertex information
	 std::vector<int> parts = gp3.getPartIDs();
	 std::vector<int> states = gp3.getPointStates();
	 // Viewer  

	 pcl::visualization::PCLVisualizer viewer("viewer");
	 viewer.addPolygonMesh(triangles);
	//  viewer.setRepresentationToWireframeForAllActors();//网格模型以线框图模式显示  
	 viewer.setRepresentationToSurfaceForAllActors(); //网格模型以面片形式显示
	// viewer.setRepresentationToPointsForAllActors(); //网格模型以点形式显示  
	 viewer.spin();
	 return Unfiltered->points.size();
 }

 //生成mesh三角面片，并保存成stl文件
 extern "C" _declspec(dllexport) int GetMarchingCubesPolygonMesh(Point3DF32* InPoints, int number)
 {
	 //load input Points
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr result(new pcl::PointCloud<pcl::PointXYZ>);//原点云

																							//将PT类型的指针转换成PointXYZ类型
	 cloud->points.resize(number);
	 for (size_t i = 0; i<number; i++)
	 {
		 cloud->points[i].x = InPoints[i].x;
		 cloud->points[i].y = InPoints[i].y;
		 cloud->points[i].z = InPoints[i].z;
	 }
	 //Resample:VoxelGrid // Create the filtering object
	/* pcl::VoxelGrid<pcl::PointXYZ> vol;
	 vol.setInputCloud(cloud);
	 vol.setLeafSize(0.003f, 0.003f, 0.003f);
	 vol.filter(*cloud_filtered);*/
	 pcl::UniformSampling < pcl::PointXYZ> unfilter;
	 unfilter.setInputCloud(cloud);
	 unfilter.setRadiusSearch(0.003);
	 unfilter.filter(*cloud_filtered);
	 //采用MovingLeastSquares方法进行平滑,并进行法向量估计
	 pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals(new pcl::PointCloud<pcl::PointNormal>);
	 pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> mls;
	 mls.setComputeNormals(true);
	 mls.setInputCloud(cloud_filtered);
	 mls.setPolynomialFit(true);
	 mls.setSearchRadius(0.03);
	 mls.process(*cloud_with_normals);
	 pcl::copyPointCloud(*cloud_with_normals,*result);
	 pcl::io::savePCDFile("E:/PLC_example/CTestDLL/test.pcd",*result);
	 //Normal estimation
	 /*pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
	 pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
	 pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
	 tree->setInputCloud(cloud_filtered);
	 ne.setInputCloud(cloud_filtered);
	 ne.setSearchMethod(tree);
	 ne.setKSearch(10);
	 ne.compute(*normals);*/
	 //* normals should not contain the point normals + surface curvatures
	 // Concatenate the XYZ and normal fields*
	 //pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals(new pcl::PointCloud<pcl::PointNormal>);
	 // pcl::concatenateFields(*cloud_filtered, *normals, *cloud_with_normals);
	 //* cloud_with_normals = cloud + normals
	 // Create search tree*
	/* pcl::search::KdTree<pcl::PointNormal>::Ptr tree2(new pcl::search::KdTree<pcl::PointNormal>);
	 tree2->setInputCloud(cloud_with_normals);
	 */// Initialize objects
	// pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
	 //pcl::PolygonMesh triangles;
	 // Set the maximum distance between connected points (maximum edge length)
	 //gp3.setSearchRadius(0.1);

	 //// Set typical values for the parameters
	 //gp3.setMu(2.0);
	 //gp3.setMaximumNearestNeighbors(100);
	 //gp3.setMaximumSurfaceAngle(M_PI / 2); // 90 degrees
	 //gp3.setMinimumAngle(M_PI / 18); // 10 degrees
	 //gp3.setMaximumAngle(2 * M_PI / 3); // 120 degrees
	 //gp3.setNormalConsistency(true);
	 //gp3.setConsistentVertexOrdering(true);
	 //// Get result
	 //gp3.setInputCloud(cloud_with_normals);
	 //gp3.setSearchMethod(tree2);
	 //gp3.reconstruct(triangles);
	 //将mesh三角面片保存为stl文件
	 //初始化MarchingCubes对象，并设置参数
	 pcl::MarchingCubes<pcl::PointNormal> *mc;
	 mc = new pcl::MarchingCubesHoppe<pcl::PointNormal>();
	 //创建搜索树
	 pcl::search::KdTree<pcl::PointNormal>::Ptr tree2(new pcl::search::KdTree<pcl::PointNormal>);
	 tree2->setInputCloud(cloud_with_normals);
	 //创建多变形网格，用于存储结果
	 pcl::PolygonMesh triangles;
	 //设置MarchingCubes对象的参数
	 mc->setIsoLevel(0.0f);
	 mc->setGridResolution(50, 50, 50);
	 mc->setPercentageExtendGrid(0.0f);
	 //设置搜索方法
	 mc->setInputCloud(cloud_with_normals);
	 mc->setSearchMethod(tree2);
	 mc->reconstruct(triangles);
	 pcl::io::savePolygonFileSTL("E:/PLC_example/CTestDLL/test.stl", triangles, false);
	 // Additional vertex information
	 //std::vector<int> parts = gp3.getPartIDs();
	 //std::vector<int> states = gp3.getPointStates();
	 // Viewer  

	 pcl::visualization::PCLVisualizer viewer("viewer");
	 viewer.addPolygonMesh(triangles);
	 viewer.setRepresentationToWireframeForAllActors();//网格模型以线框图模式显示  
	// viewer.setRepresentationToSurfaceForAllActors(); //网格模型以面片形式显示
	// viewer.setRepresentationToPointsForAllActors(); //网格模型以点形式显示  
	 viewer.spin();
	 return cloud_filtered->points.size();
 }

 extern "C" _declspec(dllexport) int GetPoissonPolygonMesh(Point3DF32* InPoints, int number)
 {
	 //load input Points
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr result(new pcl::PointCloud<pcl::PointXYZ>);//原点云

																					//将PT类型的指针转换成PointXYZ类型
	 cloud->points.resize(number);
	 for (size_t i = 0; i<number; i++)
	 {
		 cloud->points[i].x = InPoints[i].x;
		 cloud->points[i].y = InPoints[i].y;
		 cloud->points[i].z = InPoints[i].z;
	 }
	 //Resample:VoxelGrid // Create the filtering object
	 pcl::VoxelGrid<pcl::PointXYZ> vol;
	 vol.setInputCloud(cloud);
	 vol.setLeafSize(0.005f, 0.005f, 0.005f);
	 vol.filter(*cloud_filtered);
	
	 //采用MovingLeastSquares方法进行平滑
	  MovingLeastSquares<PointXYZ, PointXYZ> mls;
	  mls.setInputCloud(cloud_filtered);
	  mls.setSearchRadius(0.03);
	  mls.setPolynomialFit(true);
	  mls.setPolynomialOrder(4);
	 // mls.setUpsamplingMethod(MovingLeastSquares<PointXYZ, PointXYZ>::SAMPLE_LOCAL_PLANE);
	 // mls.setUpsamplingRadius(0.01);
	 // mls.setUpsamplingStepSize(0.01);
	  mls.setUpsamplingMethod(MovingLeastSquares<PointXYZ, PointXYZ>::VOXEL_GRID_DILATION);
	  mls.setDilationVoxelSize(0.005);
	  mls.setDilationIterations(100);
	  PointCloud<PointXYZ>::Ptr cloud_smoothed (new PointCloud<PointXYZ>());
	  mls.process(*cloud_smoothed);
	  cout << "Number of smoothed are :" <<cloud_smoothed->points.size()<< endl;
	 //法向量估计
	 pcl::NormalEstimationOMP<PointXYZ, Normal> ne;
	 pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
	 //ne.setNumberOfThreads(8);
	 tree->setInputCloud(cloud_smoothed);
	 ne.setInputCloud(cloud_smoothed);
	 ne.setSearchMethod(tree);
	 ne.setKSearch(10);
	 //ne.setRadiusSearch(0.01);
	 Eigen::Vector4f centroid;
	 compute3DCentroid(*cloud_smoothed, centroid);
	 ne.setViewPoint(centroid[0], centroid[1], centroid[2]);
 
	 PointCloud<Normal>::Ptr cloud_normals(new PointCloud<Normal>());
	 ne.compute(*cloud_normals);
	/* cout << "normal estimation complete" << endl;
	 cout << "reverse normals' direction" << endl;*/

	/* for (size_t i = 0; i < cloud_normals->size(); ++i) {
		 cloud_normals->points[i].normal_x *= -1;
		 cloud_normals->points[i].normal_y *= -1;
		 cloud_normals->points[i].normal_z *= -1;
	 }*/

	 cout << "combine points and normals" << endl;
	 PointCloud<PointNormal>::Ptr cloud_smoothed_normals(new PointCloud<PointNormal>());
	 pcl::concatenateFields(*cloud_smoothed, *cloud_normals, *cloud_smoothed_normals);
	 //创建搜索树：tree2
	 pcl::search::KdTree<pcl::PointNormal>::Ptr tree2(new pcl::search::KdTree<pcl::PointNormal>);
	 tree2->setInputCloud(cloud_smoothed_normals);
	 //创建poisson重建对象
	 pcl::Poisson<pcl::PointNormal> poisson;
	 poisson.setConfidence(false);
	 poisson.setDegree(2);
	 poisson.setDepth(8);
	 poisson.setIsoDivide(8);
	 poisson.setManifold(false);
	 poisson.setOutputPolygons(false);
	 poisson.setSamplesPerNode(3.0);
	 poisson.setScale(1.25);
	 poisson.setSolverDivide(8);
	 poisson.setSearchMethod(tree2);

	 //输入poisson重建点云数据
	 poisson.setInputCloud(cloud_smoothed_normals);
	 //创建网格对象指针，用于存储重建结果
	 pcl::PolygonMesh triangles;
	 //poisson重建开始
	 poisson.reconstruct(triangles);

	 pcl::io::savePolygonFileSTL("E:/PLC_example/CTestDLL/test.stl", triangles, false);
	 // Additional vertex information
	 //std::vector<int> parts = gp3.getPartIDs();
	 //std::vector<int> states = gp3.getPointStates();
	 // Viewer  

	 pcl::visualization::PCLVisualizer viewer("viewer");
	 viewer.addPolygonMesh(triangles);
	 //viewer.setRepresentationToWireframeForAllActors();//网格模型以线框图模式显示  
	 viewer.setRepresentationToSurfaceForAllActors();												   // viewer.setRepresentationToSurfaceForAllActors(); //网格模型以面片形式显示
	//viewer.setRepresentationToPointsForAllActors(); //网格模型以点形式显示  
	 viewer.spin();
	 return cloud_filtered->points.size();
 }
 
 extern "C" _declspec(dllexport) int GetGridProjectionPolygonMesh(Point3DF32* InPoints, int number)
 {
	 //load input Points
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);//原点云
	 pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_smoothed(new pcl::PointCloud<pcl::PointXYZ>);//原点云

																							//将PT类型的指针转换成PointXYZ类型
	 cloud->points.resize(number);
	 for (size_t i = 0; i<number; i++)
	 {
		 cloud->points[i].x = InPoints[i].x;
		 cloud->points[i].y = InPoints[i].y;
		 cloud->points[i].z = InPoints[i].z;
	 }
	 //Resample:VoxelGrid // Create the filtering object
	 pcl::VoxelGrid<pcl::PointXYZ> vol;
	 vol.setInputCloud(cloud);
	 vol.setLeafSize(0.005f, 0.005f, 0.005f);
	 vol.filter(*cloud_filtered);
	 /*MovingLeastSquares<PointXYZ, PointXYZ> mls1;
	 mls1.setInputCloud(cloud_filtered);
	 mls1.setSearchRadius(0.03);
	 mls1.setPolynomialFit(true);
	 mls1.setPolynomialOrder(4);
	 mls1.process(*cloud_smoothed);*/
	 //采用MovingLeastSquares方法进行平滑,并进行法向量估计
	 pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals(new pcl::PointCloud<pcl::PointNormal>);
	 pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> mls;
	 mls.setComputeNormals(true);
	 mls.setInputCloud(cloud_filtered);
	 mls.setPolynomialFit(true);
	 mls.setPolynomialOrder(4);
	 mls.setSearchRadius(0.05);
	 mls.process(*cloud_with_normals);
	 //创建GridProjection对象
	 pcl::GridProjection<pcl::PointNormal> gp;
	 pcl::PolygonMesh triangles;
	 gp.setInputCloud(cloud_with_normals);
	 //创建搜索树：tree2
	 pcl::search::KdTree<pcl::PointNormal>::Ptr tree2(new pcl::search::KdTree<pcl::PointNormal>);
	 tree2->setInputCloud(cloud_with_normals);
	 gp.setSearchMethod(tree2);
	 gp.setResolution(0.003);
	 gp.setPaddingSize(3);
	 gp.setMaxBinarySearchLevel(8);
	 gp.reconstruct(triangles);
	 triangles.cloud;
	 

	 //将mesh三角面片保存为stl文件
	// pcl::io::savePolygonFileSTL("E:/PLC_example/CTestDLL/test.stl", triangles, false);
	 // Viewer 
	 pcl::visualization::PCLVisualizer viewer("viewer");
	 viewer.addPolygonMesh(triangles);
	 // viewer.setRepresentationToWireframeForAllActors();//网格模型以线框图模式显示  
	 viewer.setRepresentationToSurfaceForAllActors(); //网格模型以面片形式显示
	// viewer.setRepresentationToPointsForAllActors(); //网格模型以点形式显示  
	 viewer.spin();
	 return cloud_filtered->points.size();
 }


