#pragma once
//包含的头文件
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/point_types.h>
#include <iostream>
#include <vector>
#include <ctime>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/passthrough.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>
#include <pcl/visualization/pcl_visualizer.h>  
#include <boost/math/special_functions/round.hpp> 
#include <pcl/filters/voxel_grid.h>
#include <pcl/PolygonMesh.h>
#include <pcl/io/vtk_lib_io.h>
#include <pcl/surface/mls.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/io/pcd_io.h>
#include <pcl/keypoints/uniform_sampling.h>
#include <pcl/surface/marching_cubes_hoppe.h>
#include <pcl/surface/marching_cubes_rbf.h>
#include <pcl/surface/gp3.h>
#include <pcl/surface/poisson.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/surface/grid_projection.h>

//包含的数据类型
class UserInfo {
private:
	char* m_Name;
	int m_Age;
public:
	UserInfo(char* name, int age)
	{
		m_Name = name;
		m_Age = age;
	}
	virtual ~UserInfo() { }
	int GetAge() { return m_Age; }
	char* GetName() { return m_Name; }
};
typedef struct
{
	char name[32];
	int age;

}User;

typedef struct
{
	float x;
	float y;
	float z;

}Point3DF32;

typedef struct
{
	Point3DF32 normal;
	Point3DF32 vertexs[3];
	
}STLMesh;
//typedef struct
//{
//	STLtriangles *triangles;
//	long size;
//}STLMesh;
//声明的函数
extern "C" _declspec(dllexport) int __stdcall test01(int a,int b,int c);
extern "C" _declspec(dllexport) int __stdcall test02(int a, int b);
extern "C" _declspec(dllexport) int __stdcall  test03();
